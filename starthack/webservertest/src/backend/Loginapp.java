package backend;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class server
 */
@WebServlet("/loginapp")
public class Loginapp extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	  public void init() throws ServletException
	  {
	      // Do required initialization
	      System.out.println("Hello World");
	  }
	  
    /**
     * Default constructor. 
     */
    public Loginapp() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Get request");
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String op = request.getParameter("op");
		if(op=="notapp") {
			this.getServletContext().getRequestDispatcher("/WebContent/notapp.jsp").forward( request, response );	
		}else{
			request.getRequestDispatcher("index.html").forward(request, response);
			return;	
		}		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
