import hashlib
import base64
import hmac

secret_key = "X2fUM9HMgMVjJE_VvZoSd88QQLnih2LuGchT72eu"

method = raw_input("REST Method: ")

contentMd5 = hashlib.md5(raw_input("message body: ")).digest()

content_type = raw_input("content-type: ")

date = raw_input("date: ")

canonical_resource = raw_input("resource path: ")

hash_string = method + "\n" + contentMd5 + "\n" + content_type + "\n" + date + "\n" + canonical_resource

print hash_string
print hmac.new(secret_key, hash_string).digest

signature = base64.b64encode(hmac.new(secret_key, hash_string).digest)